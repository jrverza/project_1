<div align="center"> <h1 align="center"> Insights Project</h1> </div>

## About the project
The goal of this project is to generate insights through Exploratory Data Analysis (EDA).

### Business Context
House Rocket is a fictitious company, which is a digital platform whose business model is buying and selling houses using technology. Its main strategy is to buy good houses in great locations at low prices and then resell them later at higher prices. The greater the difference between buying and selling, the greater the company's profit and therefore the greater its revenue. However, houses have many attributes that make them more or less attractive to buyers and sellers, and location and time of year can also influence prices.

### Business Issue
Given this context, we seek to answer the following questions:

    1 - Which properties should I buy and at what price ?
    2 - Once the property is purchased, when is the best time to sell it and at what price ?

### Features
The data for developing this insights project was taken from Kaggle¹. In short, this dataset is formed by rows and columns, where each column is a feature and its meaning is explained in this [link](https://www.kaggle.com/harlfoxem/housesalesprediction/discussion/207885).

¹ https://www.kaggle.com/harlfoxem/housesalesprediction/discussion/207885

### Business Premises
Some assumptions were made with a quick overview of the data:

    - the yr_renovated column with values equal to zero, means that the houses have never been renovated
    - the houses with waterfront equal to zero, means that they have no water view.
    - the floors and bathrooms columns have fractional numbers, this has no physical meaning, we should ask what this means in real estate terms.
    - ID with 33 bathrooms is an error in the table

### Solution Planning
The solution was planned as follows, it is worth mentioning that the tools used were Python 3.8, Jupyter Notebook, Jupyter Lab, Sublime Text, Streamlit and Heroku. All the libraries needed for this project are in the requiments.txt

    - Data Collection: Kaggle
    - Business understanding	
    - Data treatment: Stage carried out to transform, clean and understand the data in order to bring hypotheses to the business team.
    - Hypothesis Research
    - Data exploration: Stage that seeks to verify whether the hypotheses generated are false or true. In addition, we look for correlations between the various variables and the answer variable. Here we can also generate deeper hypotheses about the data.
    - Survey of the main Insights found
    - Translation to the business: After the hypotheses have been verified, we seek to analyze their meaning for the business itself.
    - Conclusion: Answers to the business questions
    - Deploy: Use a free cloud (heroku) to storage the project and Streamlit to create a interactive dashboard 

### Hypotheses 

    H1 = Houses prices increase 10% MoM on average
    H2 = Houses never renovated are 15% cheaper on average 
    H3 = Summer season is 30% more expensive on average than other seasons
    H4 = Water view houses in summer are 40% more expensive on average than water view houses in winter 
    H5 = Houses with water view are on average 30% more expensive

### Insights 
Only hypotheses H2 and H5 were true. With the data visualization we can observe and take away some business relevant insights, which are presented below.

    - Water view properties do not have a large seasonal dependence on price.
    - Properties with a water view are more expensive on average. 
    - Properties without renovation are cheaper on average.

### Business translation 
In this step we translate what the hypotheses say into business language:

    H1 = House prices increase 10% MoM on average: FALSE. House prices fluctuate naturally over the course of months. One should further analyze which local facts impacted these historical prices.
    H2 = Properties that have never been renovated are 15% cheaper on average: TRUE. One should invest in never-retired properties with a water view, so that we can put a higher percentage of profit on top of the sale price.
    H3 = The summer season is 30% more expensive on average than the other seasons: FALSE. We can invest in houses at any time of the year, since what influences the price are the local and physical characteristics of the property.
    H4 = Properties with water view in the summer season are on average 40% more expensive than those with water view in the winter: FALSE. We can buy and sell water view properties at any time of the year.
    H5 = Water view properties are on average 30% more expensive: TRUE. In other words, the characteristics of the location affect the price of houses. Therefore, houses with a water view will bring a higher profit if we use this location in our favor as a selling strategy.

### Deploy 
The deployment was done on Heroku and a interactive dashboard was created on Streamlit that can be acessed [here](https://house-rocket.herokuapp.com/).

### Next steps
Start another CRISP cycle to further refine the solution and answer more complex business questions that deliver more value

